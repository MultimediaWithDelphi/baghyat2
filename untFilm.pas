unit untFilm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, MPlayer;

type
  TfrmFilm = class(TForm)
    imgFilm: TImage;
    lblBack: TLabel;
    imgStop: TImage;
    lblStop: TLabel;
    imgPlay: TImage;
    imgPause: TImage;
    imgPauseS: TImage;
    lblPlayPause: TLabel;
    MediaPlayer1: TMediaPlayer;
    pnlFilm: TPanel;
    procedure lblBackClick(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFilm: TfrmFilm;
  CloseB: Boolean;

implementation

{$R *.dfm}

procedure TfrmFilm.lblBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFilm.lblStopClick(Sender: TObject);
begin
  imgPause.Visible:=False;
  imgPauseS.Visible:=False;
  imgPlay.Visible:=False;
  MediaPlayer1.Stop;
  MediaPlayer1.Position:=0;
end;

procedure TfrmFilm.lblStopMouseEnter(Sender: TObject);
begin
  imgStop.Visible:=True;
end;

procedure TfrmFilm.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible:=False;
end;

procedure TfrmFilm.lblPlayPauseClick(Sender: TObject);
begin
  if imgPause.Visible then
  begin
    imgPause.Visible:=False;
    imgPauseS.Visible:=False;
    MediaPlayer1.Pause;
  end
  else
  begin
    imgPause.Visible:=True;
    imgPauseS.Visible:=True;
    MediaPlayer1.Play;
  end;
end;

procedure TfrmFilm.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=True
  else
    imgPlay.Visible:=True;
end;

procedure TfrmFilm.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=False
  else
    imgPlay.Visible:=False;
end;

procedure TfrmFilm.FormShow(Sender: TObject);
begin
  MediaPlayer1.FileName:=frmFilm.HelpFile;
  frmFilm.HelpFile:='';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
end;

procedure TfrmFilm.MediaPlayer1Notify(Sender: TObject);
begin
  if not CloseB then
    if MediaPlayer1.Position>=MediaPlayer1.Length then
    begin
      imgPause.Visible:=False;
      imgPauseS.Visible:=False;
    end;
end;

procedure TfrmFilm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MediaPlayer1.Close;
  CloseB:=True;
end;

end.

