unit untQuran;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TfrmQuran = class(TForm)
    imgQuran: TImage;
    lblBack: TLabel;
    lblJoz: TLabel;
    lblSooreh: TLabel;
    lblAyeh: TLabel;
    imgPlay: TImage;
    lblPlay: TLabel;
    imgStop: TImage;
    lblStop: TLabel;
    imgJozPre: TImage;
    lblJozPre: TLabel;
    imgJozNext: TImage;
    lblJozNext: TLabel;
    imgSoorehPre: TImage;
    lblSoorehPre: TLabel;
    imgSoorehNext: TImage;
    lblSoorehNext: TLabel;
    imgAyehPre: TImage;
    lblAyehPre: TLabel;
    imgAyehNext: TImage;
    lblAyehNext: TLabel;
    procedure lblBackClick(Sender: TObject);
    procedure lblPlayMouseEnter(Sender: TObject);
    procedure lblPlayMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblJozPreMouseEnter(Sender: TObject);
    procedure lblJozPreMouseLeave(Sender: TObject);
    procedure lblJozNextMouseEnter(Sender: TObject);
    procedure lblJozNextMouseLeave(Sender: TObject);
    procedure lblSoorehPreMouseEnter(Sender: TObject);
    procedure lblSoorehPreMouseLeave(Sender: TObject);
    procedure lblSoorehNextMouseEnter(Sender: TObject);
    procedure lblSoorehNextMouseLeave(Sender: TObject);
    procedure lblAyehPreMouseEnter(Sender: TObject);
    procedure lblAyehPreMouseLeave(Sender: TObject);
    procedure lblAyehNextMouseEnter(Sender: TObject);
    procedure lblAyehNextMouseLeave(Sender: TObject);
    procedure lblJozPreClick(Sender: TObject);
    procedure lblJozNextClick(Sender: TObject);
    procedure lblSoorehPreClick(Sender: TObject);
    procedure lblSoorehNextClick(Sender: TObject);
    procedure lblAyehNextClick(Sender: TObject);
    procedure lblAyehPreClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmQuran: TfrmQuran;

implementation

{$R *.dfm}

procedure TfrmQuran.lblBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmQuran.lblPlayMouseEnter(Sender: TObject);
begin
  imgPlay.Visible:=True;
end;

procedure TfrmQuran.lblPlayMouseLeave(Sender: TObject);
begin
  imgPlay.Visible:=False;
end;

procedure TfrmQuran.lblStopMouseEnter(Sender: TObject);
begin
  imgStop.Visible:=True;
end;

procedure TfrmQuran.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible:=False;
end;

procedure TfrmQuran.lblJozPreMouseEnter(Sender: TObject);
begin
  imgJozPre.Visible:=True;
end;

procedure TfrmQuran.lblJozPreMouseLeave(Sender: TObject);
begin
  imgJozPre.Visible:=False;
end;

procedure TfrmQuran.lblJozNextMouseEnter(Sender: TObject);
begin
  imgJozNext.Visible:=True;
end;

procedure TfrmQuran.lblJozNextMouseLeave(Sender: TObject);
begin
  imgJozNext.Visible:=False;
end;

procedure TfrmQuran.lblSoorehPreMouseEnter(Sender: TObject);
begin
  imgSoorehPre.Visible:=True;
end;

procedure TfrmQuran.lblSoorehPreMouseLeave(Sender: TObject);
begin
  imgSoorehPre.Visible:=False;
end;

procedure TfrmQuran.lblSoorehNextMouseEnter(Sender: TObject);
begin
  imgSoorehNext.Visible:=True;
end;

procedure TfrmQuran.lblSoorehNextMouseLeave(Sender: TObject);
begin
  imgSoorehNext.Visible:=False;
end;

procedure TfrmQuran.lblAyehPreMouseEnter(Sender: TObject);
begin
  imgAyehPre.Visible:=True;
end;

procedure TfrmQuran.lblAyehPreMouseLeave(Sender: TObject);
begin
  imgAyehPre.Visible:=False;
end;

procedure TfrmQuran.lblAyehNextMouseEnter(Sender: TObject);
begin
  imgAyehNext.Visible:=True;
end;

procedure TfrmQuran.lblAyehNextMouseLeave(Sender: TObject);
begin
  imgAyehNext.Visible:=False;
end;

procedure TfrmQuran.lblJozPreClick(Sender: TObject);
var
  t: Integer;
begin
  lblJozNext.Visible:=True;
  t:=StrToInt(lblJoz.Caption)-1;
  if t<=1 then
    lblJozPre.Visible:=False;
  lblJoz.Caption:=IntToStr(t);
  // Joz Variable must be valued with 't'.
end;

procedure TfrmQuran.lblJozNextClick(Sender: TObject);
var
  t: Integer;
begin
  lblJozPre.Visible:=True;
  t:=StrToInt(lblJoz.Caption)+1;
  if t>=30 then
    lblJozNext.Visible:=False;
  lblJoz.Caption:=IntToStr(t);
  // Joz Variable must be valued with 't'.
end;

procedure TfrmQuran.lblSoorehPreClick(Sender: TObject);
var
  t: Integer;
begin
  lblSoorehNext.Visible:=True;
  t:=StrToInt(lblSooreh.Caption)-1;
  if t<=1 then
    lblSoorehPre.Visible:=False;
  lblSooreh.Caption:=IntToStr(t);
  // Sooreh Variable must be valued with 't'.
end;

procedure TfrmQuran.lblSoorehNextClick(Sender: TObject);
var
  t: Integer;
begin
  lblSoorehPre.Visible:=True;
  t:=StrToInt(lblSooreh.Caption)+1;
  if t>=114 then
    lblSoorehNext.Visible:=False;
  lblSooreh.Caption:=IntToStr(t);
  // Sooreh Variable must be valued with 't'.
end;

procedure TfrmQuran.lblAyehNextClick(Sender: TObject);
var
  t: Integer;
begin
  lblAyehPre.Visible:=True;
  t:=StrToInt(lblAyeh.Caption)+1;
  if t>=7 then
    lblAyehNext.Visible:=False;
  lblAyeh.Caption:=IntToStr(t);
  // Ayeh Variable must be valued with 't'.
end;


procedure TfrmQuran.lblAyehPreClick(Sender: TObject);
var
  t: Integer;
begin
  lblAyehNext.Visible:=True;
  t:=StrToInt(lblAyeh.Caption)-1;
  if t<=1 then
    lblAyehPre.Visible:=False;
  lblAyeh.Caption:=IntToStr(t);
  // Ayeh Variable must be valued with 't'.
end;

end.
