program Baghyat2;

uses
  Forms,
  untMain in 'untMain.pas' {frmMain},
  untBackground in 'untBackground.pas' {frmBackground},
  untQuran in 'untQuran.pas' {frmQuran},
  untMolodi in 'untMolodi.pas' {frmMolodi},
  untAdeieh in 'untAdeieh.pas' {frmAdeieh},
  untFilm in 'untFilm.pas' {frmFilm},
  untHadieh in 'untHadieh.pas' {frmHadieh};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmBackground, frmBackground);
  Application.CreateForm(TfrmQuran, frmQuran);
  Application.CreateForm(TfrmMolodi, frmMolodi);
  Application.CreateForm(TfrmAdeieh, frmAdeieh);
  Application.CreateForm(TfrmFilm, frmFilm);
  Application.CreateForm(TfrmHadieh, frmHadieh);
  Application.Run;
end.
