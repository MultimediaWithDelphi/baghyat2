unit untMolodi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, MPlayer, ComCtrls;

type
  TfrmMolodi = class(TForm)
    imgMolodi: TImage;
    lblBack: TLabel;
    imgStop: TImage;
    lblStop: TLabel;
    imgPlay: TImage;
    imgPause: TImage;
    imgPauseS: TImage;
    lblPlayPause: TLabel;
    lbl1Rasool: TLabel;
    lbl2Ali: TLabel;
    lbl3Fatemeh: TLabel;
    lbl4Hasan: TLabel;
    lbl5Hossein: TLabel;
    lbl6Sajad: TLabel;
    lbl7Bagher: TLabel;
    lbl8Sadegh: TLabel;
    lbl9Kazem: TLabel;
    lbl10Reza: TLabel;
    lbl11Javad: TLabel;
    lbl12Hadi: TLabel;
    lbl13Askari: TLabel;
    lbl14Mahdi: TLabel;
    imgName: TImage;
    MediaPlayer1: TMediaPlayer;
    Animate1: TAnimate;
    Panel1: TPanel;
    procedure lblBackClick(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbl1RasoolMouseEnter(Sender: TObject);
    procedure lbl2AliMouseEnter(Sender: TObject);
    procedure lbl3FatemehMouseEnter(Sender: TObject);
    procedure lbl4HasanMouseEnter(Sender: TObject);
    procedure lbl5HosseinMouseEnter(Sender: TObject);
    procedure lbl6SajadMouseEnter(Sender: TObject);
    procedure lbl7BagherMouseEnter(Sender: TObject);
    procedure lbl8SadeghMouseEnter(Sender: TObject);
    procedure lbl9KazemMouseEnter(Sender: TObject);
    procedure lbl10RezaMouseEnter(Sender: TObject);
    procedure lbl11JavadMouseEnter(Sender: TObject);
    procedure lbl12HadiMouseEnter(Sender: TObject);
    procedure lbl13AskariMouseEnter(Sender: TObject);
    procedure lbl14MahdiMouseEnter(Sender: TObject);
    procedure lbl1RasoolClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbl1RasoolMouseLeave(Sender: TObject);
    procedure lbl2AliClick(Sender: TObject);
    procedure lbl3FatemehClick(Sender: TObject);
    procedure lbl4HasanClick(Sender: TObject);
    procedure lbl5HosseinClick(Sender: TObject);
    procedure lbl6SajadClick(Sender: TObject);
    procedure lbl7BagherClick(Sender: TObject);
    procedure lbl8SadeghClick(Sender: TObject);
    procedure lbl9KazemClick(Sender: TObject);
    procedure lbl10RezaClick(Sender: TObject);
    procedure lbl11JavadClick(Sender: TObject);
    procedure lbl12HadiClick(Sender: TObject);
    procedure lbl13AskariClick(Sender: TObject);
    procedure lbl14MahdiClick(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMolodi: TfrmMolodi;
  MNo: Byte;
  CloseB: Boolean;
  Disp: TRect;

implementation

uses Types;

{$R *.dfm}

procedure TfrmMolodi.lblBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMolodi.lblPlayPauseClick(Sender: TObject);
begin
  if imgPause.Visible then
  begin
    imgPause.Visible:=False;
    imgPauseS.Visible:=False;
    MediaPlayer1.Pause;
  end
  else
  begin
    imgPause.Visible:=True;
    imgPauseS.Visible:=True;
    imgPlay.Visible:=False;
    MediaPlayer1.Play;
  end;
end;

procedure TfrmMolodi.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=True
  else
    imgPlay.Visible:=True;
end;

procedure TfrmMolodi.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=False
  else
    imgPlay.Visible:=False;
end;

procedure TfrmMolodi.lblStopClick(Sender: TObject);
begin
  imgPause.Visible:=False;
  imgPauseS.Visible:=False;
  imgPlay.Visible:=False;
  MediaPlayer1.Stop;
  MediaPlayer1.Position:=0;
end;

procedure TfrmMolodi.lblStopMouseEnter(Sender: TObject);
begin
  imgStop.Visible:=True;
end;

procedure TfrmMolodi.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible:=False;
end;

procedure TfrmMolodi.FormShow(Sender: TObject);
begin
  imgPause.Visible:=False;
  imgPauseS.Visible:=False;
  imgPlay.Visible:=False;
  CloseB:=True;
  Animate1.Visible:=False;
//  Unvisible;
  MNo:=0;
end;

procedure TfrmMolodi.lbl1RasoolMouseEnter(Sender: TObject);
begin
  Animate1.Visible:=False;
  imgName.Picture.LoadFromFile('Data\1.jpg');
end;

procedure TfrmMolodi.lbl2AliMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\2.jpg');
end;

procedure TfrmMolodi.lbl3FatemehMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\3.jpg');
end;

procedure TfrmMolodi.lbl4HasanMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\4.jpg');
end;

procedure TfrmMolodi.lbl5HosseinMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\5.jpg');
end;

procedure TfrmMolodi.lbl6SajadMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\6.jpg');
end;

procedure TfrmMolodi.lbl7BagherMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\7.jpg');
end;

procedure TfrmMolodi.lbl8SadeghMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\8.jpg');
end;

procedure TfrmMolodi.lbl9KazemMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\9.jpg');
end;

procedure TfrmMolodi.lbl10RezaMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\10.jpg');
end;

procedure TfrmMolodi.lbl11JavadMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\11.jpg');
end;

procedure TfrmMolodi.lbl12HadiMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\12.jpg');
end;

procedure TfrmMolodi.lbl13AskariMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\13.jpg');
end;

procedure TfrmMolodi.lbl14MahdiMouseEnter(Sender: TObject);
begin
  imgName.Picture.LoadFromFile('Data\14.jpg');
end;

procedure TfrmMolodi.lbl1RasoolClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\1.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=1;
end;

procedure TfrmMolodi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MediaPlayer1.Close;
  CloseB:=True;
  Animate1.Visible:=False;
  lblPlayPause.Visible:=False;
  lblStop.Visible:=False;
  imgName.Picture.Bitmap.FreeImage;
end;

procedure TfrmMolodi.lbl1RasoolMouseLeave(Sender: TObject);
begin
//  Animate1.Visible:=True;
end;

procedure TfrmMolodi.lbl2AliClick(Sender: TObject);
var
  tp: TPoint;
  tc: TCanvas;
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\2.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  with Disp do
  begin
    Left:=192;
    Top:=32;
    Right:=529;
    Bottom:=297;
  end;
  tc.Rectangle(Disp);
{  CreateRectRgn(192,32,529,297);
  MediaPlayer1.DisplayRect:=Disp;
}  MediaPlayer1.PaintTo(tc,192,32);
//  MediaPlayer1.ScaleBy(110,110);
  MediaPlayer1.Play;
  with tp do
  begin
    X:=192;
    Y:=32;
  end;
  MediaPlayer1.ScreenToClient(tp);
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=2;
end;

procedure TfrmMolodi.lbl3FatemehClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\2.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Panel1.Visible:=False;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=3;
end;

procedure TfrmMolodi.lbl4HasanClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\4.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=4;
end;

procedure TfrmMolodi.lbl5HosseinClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\5.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=5;
end;

procedure TfrmMolodi.lbl6SajadClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\6.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=6;
end;

procedure TfrmMolodi.lbl7BagherClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\7.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=7;
end;

procedure TfrmMolodi.lbl8SadeghClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\8.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=8;
end;

procedure TfrmMolodi.lbl9KazemClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\9.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=9;
end;

procedure TfrmMolodi.lbl10RezaClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\10.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=10;
end;

procedure TfrmMolodi.lbl11JavadClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\11.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=11;
end;

procedure TfrmMolodi.lbl12HadiClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\12.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=12;
end;

procedure TfrmMolodi.lbl13AskariClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\13.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=13;
end;

procedure TfrmMolodi.lbl14MahdiClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datam\14.asf';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
//  Unvisible;
//  img1Salamati.Visible:=True;
  MNo:=14;
end;

procedure TfrmMolodi.MediaPlayer1Notify(Sender: TObject);
begin
  if not CloseB then
    if MediaPlayer1.Position>=MediaPlayer1.Length then
    begin
      imgPause.Visible:=False;
      imgPauseS.Visible:=False;
      MediaPlayer1.Rewind;
    end;
end;

end.
