unit untHadieh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TfrmHadieh = class(TForm)
    imgHadieh: TImage;
    lblBack: TLabel;
    imgDesktop: TImage;
    lblDesktop: TLabel;
    lblTime: TLabel;
    imgTimdDn: TImage;
    imgTimeUp: TImage;
    lblTimeDn: TLabel;
    lblTimeUp: TLabel;
    Timer1: TTimer;
    imgPicPre: TImage;
    imgPause: TImage;
    imgPlay: TImage;
    imgPlayS: TImage;
    imgPicNext: TImage;
    lblPicPre: TLabel;
    lblPlayPause: TLabel;
    lblPicNext: TLabel;
    procedure lblBackClick(Sender: TObject);
    procedure lblDesktopMouseEnter(Sender: TObject);
    procedure lblDesktopMouseLeave(Sender: TObject);
    procedure lblTimeUpClick(Sender: TObject);
    procedure lblTimeDnClick(Sender: TObject);
    procedure lblTimeUpMouseEnter(Sender: TObject);
    procedure lblTimeUpMouseLeave(Sender: TObject);
    procedure lblTimeDnMouseEnter(Sender: TObject);
    procedure lblTimeDnMouseLeave(Sender: TObject);
    procedure lblPicNextMouseEnter(Sender: TObject);
    procedure lblPicNextMouseLeave(Sender: TObject);
    procedure lblPicPreMouseEnter(Sender: TObject);
    procedure lblPicPreMouseLeave(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHadieh: TfrmHadieh;

implementation

{$R *.dfm}

procedure TfrmHadieh.lblBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmHadieh.lblDesktopMouseEnter(Sender: TObject);
begin
  imgDesktop.Visible:=True;
end;

procedure TfrmHadieh.lblDesktopMouseLeave(Sender: TObject);
begin
  imgDesktop.Visible:=False;
end;

procedure TfrmHadieh.lblTimeUpClick(Sender: TObject);
var
 t: Integer;
begin
  if not(lblTimeDn.Visible) then
    lblTimeDn.Visible:=True;
  t:=StrToInt(lblTime.Caption)+1;
  if t>=20 then
    lblTimeUp.Visible:=False;
  if t<=20 then
  begin
    lblTime.Caption:=IntToStr(t);
    Timer1.Interval:=t*1000;
  end;
end;

procedure TfrmHadieh.lblTimeDnClick(Sender: TObject);
var
 t: Integer;
begin
  if not(lblTimeUp.Visible) then
    lblTimeUp.Visible:=True;
  t:=StrToInt(lblTime.Caption)-1;
  if t<=5 then
    lblTimeDn.Visible:=False;
  if t>=5 then
  begin
    lblTime.Caption:=IntToStr(t);
    Timer1.Interval:=t*1000;
  end;
end;

procedure TfrmHadieh.lblTimeUpMouseEnter(Sender: TObject);
begin
  imgTimeUp.Visible:=True;
end;

procedure TfrmHadieh.lblTimeUpMouseLeave(Sender: TObject);
begin
  imgTimeUp.Visible:=False;
end;

procedure TfrmHadieh.lblTimeDnMouseEnter(Sender: TObject);
begin
  imgTimdDn.Visible:=True;
end;

procedure TfrmHadieh.lblTimeDnMouseLeave(Sender: TObject);
begin
  imgTimdDn.Visible:=False;
end;

procedure TfrmHadieh.lblPicNextMouseEnter(Sender: TObject);
begin
  imgPicNext.Visible:=True;
end;

procedure TfrmHadieh.lblPicNextMouseLeave(Sender: TObject);
begin
  imgPicNext.Visible:=False;
end;

procedure TfrmHadieh.lblPicPreMouseEnter(Sender: TObject);
begin
  imgPicPre.Visible:=True;
end;

procedure TfrmHadieh.lblPicPreMouseLeave(Sender: TObject);
begin
  imgPicPre.Visible:=False;
end;

procedure TfrmHadieh.lblPlayPauseClick(Sender: TObject);
begin
  if imgPlay.Visible then
  begin
    imgPause.Visible:=True;
    imgPlay.Visible:=False;
    imgPlayS.Visible:=False;
    lblPicPre.Visible:=False;
    lblPicNext.Visible:=False;
  end
  else
  begin
    imgPlay.Visible:=True;
    imgPlayS.Visible:=True;
    imgPause.Visible:=False;
    lblPicPre.Visible:=True;
    lblPicNext.Visible:=True;
  end;
end;

procedure TfrmHadieh.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if not(imgPlay.Visible) then
    imgPause.Visible:=True
  else
    imgPlayS.Visible:=True;
end;

procedure TfrmHadieh.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if not(imgPlay.Visible) then
    imgPause.Visible:=False
  else
    imgPlayS.Visible:=False;
end;

procedure TfrmHadieh.FormShow(Sender: TObject);
begin
  imgPlay.Visible:=False;
  imgPlayS.Visible:=False;
end;

end.
