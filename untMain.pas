unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls;

type
  TfrmMain = class(TForm)
    imgMain: TImage;
    img1Quran: TImage;
    img2Molodi: TImage;
    img3Adieih: TImage;
    img4Film: TImage;
    img5Hadieh: TImage;
    img6About: TImage;
    img7Exit: TImage;
    lbl1Quran: TLabel;
    lbl2Molodi: TLabel;
    lbl3Adeieh: TLabel;
    lbl4Film: TLabel;
    lbl5Hadieh: TLabel;
    lbl6About: TLabel;
    lbl7Exit: TLabel;
    pnlFilms: TPanel;
    imgFilms: TImage;
    imgM1: TImage;
    lblM1: TLabel;
    imgM2: TImage;
    lblM2: TLabel;
    imgM3: TImage;
    lblM3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure lbl1QuranMouseEnter(Sender: TObject);
    procedure lbl1QuranMouseLeave(Sender: TObject);
    procedure lbl2MolodiMouseEnter(Sender: TObject);
    procedure lbl2MolodiMouseLeave(Sender: TObject);
    procedure lbl3AdeiehMouseEnter(Sender: TObject);
    procedure lbl3AdeiehMouseLeave(Sender: TObject);
    procedure lbl4FilmMouseEnter(Sender: TObject);
    procedure lbl4FilmMouseLeave(Sender: TObject);
    procedure lbl5HadiehMouseEnter(Sender: TObject);
    procedure lbl5HadiehMouseLeave(Sender: TObject);
    procedure lbl6AboutMouseEnter(Sender: TObject);
    procedure lbl6AboutMouseLeave(Sender: TObject);
    procedure lbl7ExitMouseEnter(Sender: TObject);
    procedure lbl7ExitMouseLeave(Sender: TObject);
    procedure lbl7ExitClick(Sender: TObject);
    procedure lbl1QuranClick(Sender: TObject);
    procedure lbl2MolodiClick(Sender: TObject);
    procedure lbl3AdeiehClick(Sender: TObject);
    procedure lbl4FilmClick(Sender: TObject);
    procedure lbl5HadiehClick(Sender: TObject);
    procedure imgMainClick(Sender: TObject);
    procedure lblM1Click(Sender: TObject);
    procedure lblM1MouseEnter(Sender: TObject);
    procedure lblM1MouseLeave(Sender: TObject);
    procedure lblM2Click(Sender: TObject);
    procedure lblM2MouseEnter(Sender: TObject);
    procedure lblM2MouseLeave(Sender: TObject);
    procedure lblM3Click(Sender: TObject);
    procedure lblM3MouseEnter(Sender: TObject);
    procedure lblM3MouseLeave(Sender: TObject);
    procedure Films;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  FilmB: Boolean;

implementation

uses untBackground, untQuran, untMolodi, untAdeieh, untFilm, untHadieh;

{$R *.dfm}

procedure TfrmMain.FormShow(Sender: TObject);
begin
  frmBackground.Show;
  frmBackground.Enabled:=False;
  Films;
end;

procedure TfrmMain.lbl1QuranMouseEnter(Sender: TObject);
begin
  Films;
  img1Quran.Visible:=True;
end;

procedure TfrmMain.lbl1QuranMouseLeave(Sender: TObject);
begin
  img1Quran.Visible:=False;
end;

procedure TfrmMain.lbl2MolodiMouseEnter(Sender: TObject);
begin
  Films;
  img2Molodi.Visible:=True;
end;

procedure TfrmMain.lbl2MolodiMouseLeave(Sender: TObject);
begin
  img2Molodi.Visible:=False;
end;

procedure TfrmMain.lbl3AdeiehMouseEnter(Sender: TObject);
begin
  Films;
  img3Adieih.Visible:=True;
end;

procedure TfrmMain.lbl3AdeiehMouseLeave(Sender: TObject);
begin
  img3Adieih.Visible:=False;
end;

procedure TfrmMain.lbl4FilmMouseEnter(Sender: TObject);
begin
  img4Film.Visible:=True;
end;

procedure TfrmMain.lbl4FilmMouseLeave(Sender: TObject);
begin
  if not FilmB then
    img4Film.Visible:=False;
end;

procedure TfrmMain.lbl5HadiehMouseEnter(Sender: TObject);
begin
  Films;
  img5Hadieh.Visible:=True;
end;

procedure TfrmMain.lbl5HadiehMouseLeave(Sender: TObject);
begin
  img5Hadieh.Visible:=False;
end;

procedure TfrmMain.lbl6AboutMouseEnter(Sender: TObject);
begin
  Films;
  img6About.Visible:=True;
end;

procedure TfrmMain.lbl6AboutMouseLeave(Sender: TObject);
begin
  img6About.Visible:=False;
end;

procedure TfrmMain.lbl7ExitMouseEnter(Sender: TObject);
begin
  Films;
  img7Exit.Visible:=True;
end;

procedure TfrmMain.lbl7ExitMouseLeave(Sender: TObject);
begin
  img7Exit.Visible:=False;
end;

procedure TfrmMain.lbl7ExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.lbl1QuranClick(Sender: TObject);
begin
  frmQuran.ShowModal;
end;

procedure TfrmMain.lbl2MolodiClick(Sender: TObject);
begin
  frmMolodi.ShowModal;
end;

procedure TfrmMain.lbl3AdeiehClick(Sender: TObject);
begin
  frmAdeieh.ShowModal;
end;

procedure TfrmMain.lbl4FilmClick(Sender: TObject);
begin
//  frmFilm.ShowModal;
  pnlFilms.Visible:=True;
  FilmB:=True;
end;

procedure TfrmMain.lbl5HadiehClick(Sender: TObject);
begin
  frmHadieh.ShowModal;
end;

procedure TfrmMain.imgMainClick(Sender: TObject);
begin
  Films;
end;

procedure TfrmMain.lblM1Click(Sender: TObject);
begin
  Films;
  frmFilm.HelpFile:='Datam\m1.asf';
  frmFilm.ShowModal;
end;

procedure TfrmMain.lblM1MouseEnter(Sender: TObject);
begin
  imgM1.Visible:=True;
end;

procedure TfrmMain.lblM1MouseLeave(Sender: TObject);
begin
  imgM1.Visible:=False;
end;

procedure TfrmMain.lblM2Click(Sender: TObject);
begin
  Films;
  frmFilm.HelpFile:='Datam\m2.asf';
  frmFilm.ShowModal;
end;

procedure TfrmMain.lblM2MouseEnter(Sender: TObject);
begin
  imgM2.Visible:=True;
end;

procedure TfrmMain.lblM2MouseLeave(Sender: TObject);
begin
  imgM2.Visible:=False;
end;

procedure TfrmMain.lblM3Click(Sender: TObject);
begin
  Films;
  frmFilm.HelpFile:='Datam\m3.asf';
  frmFilm.ShowModal;
end;

procedure TfrmMain.lblM3MouseEnter(Sender: TObject);
begin
  imgM3.Visible:=True;
end;

procedure TfrmMain.lblM3MouseLeave(Sender: TObject);
begin
  imgM3.Visible:=False;
end;

procedure TfrmMain.Films;
begin
  pnlFilms.Visible:=False;
  img4Film.Visible:=False;
  FilmB:=False;
end;
end.
