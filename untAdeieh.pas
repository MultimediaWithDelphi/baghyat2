unit untAdeieh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, MPlayer;

type
  TfrmAdeieh = class(TForm)
    imgAdeieh: TImage;
    lblBack: TLabel;
    img1Salamati: TImage;
    img2Yasin: TImage;
    img3Amin: TImage;
    img4Ashoora: TImage;
    img5Tavassol: TImage;
    img6Ahd: TImage;
    img7Mojir: TImage;
    img8Jamee: TImage;
    img9Amir: TImage;
    img10Shabanieh: TImage;
    img11AbooHamzeh: TImage;
    img12Komeil: TImage;
    img13Samat: TImage;
    img14Nodbeh: TImage;
    lbl1Salamati: TLabel;
    lbl2Yasin: TLabel;
    lbl3Amin: TLabel;
    lbl4Ashoora: TLabel;
    lbl5Tavassol: TLabel;
    lbl6Ahd: TLabel;
    lbl7Mojir: TLabel;
    lbl8Jamee: TLabel;
    lbl9Amir: TLabel;
    lbl10Shabanieh: TLabel;
    lbl11AbooHamzeh: TLabel;
    lbl12Komeil: TLabel;
    lbl13Samat: TLabel;
    lbl14Nodbeh: TLabel;
    imgStop: TImage;
    imgPlay: TImage;
    imgPause: TImage;
    imgPauseS: TImage;
    lblPlayPause: TLabel;
    lblStop: TLabel;
    MediaPlayer1: TMediaPlayer;
    procedure lblBackClick(Sender: TObject);
    procedure lbl1SalamatiMouseEnter(Sender: TObject);
    procedure lbl1SalamatiMouseLeave(Sender: TObject);
    procedure lbl2YasinMouseEnter(Sender: TObject);
    procedure lbl2YasinMouseLeave(Sender: TObject);
    procedure lbl3AminMouseEnter(Sender: TObject);
    procedure lbl3AminMouseLeave(Sender: TObject);
    procedure lbl4AshooraMouseEnter(Sender: TObject);
    procedure lbl4AshooraMouseLeave(Sender: TObject);
    procedure lbl5TavassolMouseEnter(Sender: TObject);
    procedure lbl5TavassolMouseLeave(Sender: TObject);
    procedure lbl6AhdMouseEnter(Sender: TObject);
    procedure lbl6AhdMouseLeave(Sender: TObject);
    procedure lbl7MojirMouseEnter(Sender: TObject);
    procedure lbl7MojirMouseLeave(Sender: TObject);
    procedure lbl8JameeMouseEnter(Sender: TObject);
    procedure lbl8JameeMouseLeave(Sender: TObject);
    procedure lbl9AmirMouseEnter(Sender: TObject);
    procedure lbl9AmirMouseLeave(Sender: TObject);
    procedure lbl10ShabaniehMouseEnter(Sender: TObject);
    procedure lbl10ShabaniehMouseLeave(Sender: TObject);
    procedure lbl11AbooHamzehMouseEnter(Sender: TObject);
    procedure lbl11AbooHamzehMouseLeave(Sender: TObject);
    procedure lbl12KomeilMouseEnter(Sender: TObject);
    procedure lbl12KomeilMouseLeave(Sender: TObject);
    procedure lbl13SamatMouseEnter(Sender: TObject);
    procedure lbl13SamatMouseLeave(Sender: TObject);
    procedure lbl14NodbehMouseEnter(Sender: TObject);
    procedure lbl14NodbehMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbl1SalamatiClick(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbl2YasinClick(Sender: TObject);
    procedure lbl3AminClick(Sender: TObject);
    procedure lbl4AshooraClick(Sender: TObject);
    procedure lbl5TavassolClick(Sender: TObject);
    procedure lbl6AhdClick(Sender: TObject);
    procedure lbl7MojirClick(Sender: TObject);
    procedure lbl8JameeClick(Sender: TObject);
    procedure lbl9AmirClick(Sender: TObject);
    procedure lbl10ShabaniehClick(Sender: TObject);
    procedure lbl11AbooHamzehClick(Sender: TObject);
    procedure lbl12KomeilClick(Sender: TObject);
    procedure lbl13SamatClick(Sender: TObject);
    procedure lbl14NodbehClick(Sender: TObject);
    procedure Unvisible;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdeieh: TfrmAdeieh;
  CloseB: Boolean;          // True: Close,  False: Open
  DoaNo: Byte;

implementation

{$R *.dfm}

procedure TfrmAdeieh.lblBackClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAdeieh.lbl1SalamatiMouseEnter(Sender: TObject);
begin
  img1Salamati.Visible:=True;
end;

procedure TfrmAdeieh.lbl1SalamatiMouseLeave(Sender: TObject);
begin
  if DoaNo<>1 then
    img1Salamati.Visible:=False;
end;

procedure TfrmAdeieh.lbl2YasinMouseEnter(Sender: TObject);
begin
  img2Yasin.Visible:=True;
end;

procedure TfrmAdeieh.lbl2YasinMouseLeave(Sender: TObject);
begin
  if DoaNo<>2 then
    img2Yasin.Visible:=False;
end;

procedure TfrmAdeieh.lbl3AminMouseEnter(Sender: TObject);
begin
  img3Amin.Visible:=True;
end;

procedure TfrmAdeieh.lbl3AminMouseLeave(Sender: TObject);
begin
  if DoaNo<>3 then
    img3Amin.Visible:=False;
end;

procedure TfrmAdeieh.lbl4AshooraMouseEnter(Sender: TObject);
begin
  img4Ashoora.Visible:=True;
end;

procedure TfrmAdeieh.lbl4AshooraMouseLeave(Sender: TObject);
begin
  if DoaNo<>4 then
    img4Ashoora.Visible:=False;
end;

procedure TfrmAdeieh.lbl5TavassolMouseEnter(Sender: TObject);
begin
  img5Tavassol.Visible:=True;
end;

procedure TfrmAdeieh.lbl5TavassolMouseLeave(Sender: TObject);
begin
  if DoaNo<>5 then
    img5Tavassol.Visible:=False;
end;

procedure TfrmAdeieh.lbl6AhdMouseEnter(Sender: TObject);
begin
  img6Ahd.Visible:=True;
end;

procedure TfrmAdeieh.lbl6AhdMouseLeave(Sender: TObject);
begin
  if DoaNo<>6 then
    img6Ahd.Visible:=False;
end;

procedure TfrmAdeieh.lbl7MojirMouseEnter(Sender: TObject);
begin
  img7Mojir.Visible:=True;
end;

procedure TfrmAdeieh.lbl7MojirMouseLeave(Sender: TObject);
begin
  if DoaNo<>7 then
    img7Mojir.Visible:=False;
end;

procedure TfrmAdeieh.lbl8JameeMouseEnter(Sender: TObject);
begin
  img8Jamee.Visible:=True;
end;

procedure TfrmAdeieh.lbl8JameeMouseLeave(Sender: TObject);
begin
  if DoaNo<>8 then
    img8Jamee.Visible:=False;
end;

procedure TfrmAdeieh.lbl9AmirMouseEnter(Sender: TObject);
begin
  img9Amir.Visible:=True;
end;

procedure TfrmAdeieh.lbl9AmirMouseLeave(Sender: TObject);
begin
  if DoaNo<>9 then
    img9Amir.Visible:=False;
end;

procedure TfrmAdeieh.lbl10ShabaniehMouseEnter(Sender: TObject);
begin
  img10Shabanieh.Visible:=True;
end;

procedure TfrmAdeieh.lbl10ShabaniehMouseLeave(Sender: TObject);
begin
  if DoaNo<>10 then
    img10Shabanieh.Visible:=False;
end;

procedure TfrmAdeieh.lbl11AbooHamzehMouseEnter(Sender: TObject);
begin
  img11AbooHamzeh.Visible:=True;
end;

procedure TfrmAdeieh.lbl11AbooHamzehMouseLeave(Sender: TObject);
begin
  if DoaNo<>11 then
    img11AbooHamzeh.Visible:=False;
end;

procedure TfrmAdeieh.lbl12KomeilMouseEnter(Sender: TObject);
begin
  img12Komeil.Visible:=True;
end;

procedure TfrmAdeieh.lbl12KomeilMouseLeave(Sender: TObject);
begin
  if DoaNo<>12 then
    img12Komeil.Visible:=False;
end;

procedure TfrmAdeieh.lbl13SamatMouseEnter(Sender: TObject);
begin
  img13Samat.Visible:=True;
end;

procedure TfrmAdeieh.lbl13SamatMouseLeave(Sender: TObject);
begin
  if DoaNo<>13 then
    img13Samat.Visible:=False;
end;

procedure TfrmAdeieh.lbl14NodbehMouseEnter(Sender: TObject);
begin
  img14Nodbeh.Visible:=True;
end;

procedure TfrmAdeieh.lbl14NodbehMouseLeave(Sender: TObject);
begin
  if DoaNo<>14 then
    img14Nodbeh.Visible:=False;
end;

procedure TfrmAdeieh.lblStopMouseEnter(Sender: TObject);
begin
  imgStop.Visible:=True;
end;

procedure TfrmAdeieh.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible:=False;
end;

procedure TfrmAdeieh.lblPlayPauseClick(Sender: TObject);
begin
  if imgPause.Visible then
  begin
    imgPause.Visible:=False;
    imgPauseS.Visible:=False;
    MediaPlayer1.Pause;
  end
  else
  begin
    imgPause.Visible:=True;
    imgPauseS.Visible:=True;
    MediaPlayer1.Play;
  end;
end;

procedure TfrmAdeieh.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=True
  else
    imgPlay.Visible:=True;
end;

procedure TfrmAdeieh.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPause.Visible then
    imgPauseS.Visible:=False
  else
    imgPlay.Visible:=False;
end;

procedure TfrmAdeieh.lblStopClick(Sender: TObject);
begin
  imgPause.Visible:=False;
  imgPauseS.Visible:=False;
  imgPlay.Visible:=False;
  MediaPlayer1.Stop;
  MediaPlayer1.Position:=0;
end;

procedure TfrmAdeieh.FormShow(Sender: TObject);
begin
  imgPause.Visible:=False;
  imgPlay.Visible:=False;
  CloseB:=True;
  Unvisible;
  DoaNo:=0;
end;

procedure TfrmAdeieh.lbl1SalamatiClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\1.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img1Salamati.Visible:=True;
  DoaNo:=1;
end;

procedure TfrmAdeieh.MediaPlayer1Notify(Sender: TObject);
begin
  if not CloseB then
    if MediaPlayer1.Position>=MediaPlayer1.Length then
    begin
      imgPause.Visible:=False;
      imgPauseS.Visible:=False;
    end;
end;

procedure TfrmAdeieh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MediaPlayer1.Close;
  CloseB:=True;
  lblPlayPause.Visible:=False;
  lblStop.Visible:=False;
end;

procedure TfrmAdeieh.lbl2YasinClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\2.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img2Yasin.Visible:=True;
  DoaNo:=2;
end;

procedure TfrmAdeieh.lbl3AminClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\3.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img3Amin.Visible:=True;
  DoaNo:=3;
end;

procedure TfrmAdeieh.lbl4AshooraClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\4.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img4Ashoora.Visible:=True;
  DoaNo:=4;
end;

procedure TfrmAdeieh.lbl5TavassolClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\5.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img5Tavassol.Visible:=True;
  DoaNo:=5;
end;

procedure TfrmAdeieh.lbl6AhdClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\6.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img6Ahd.Visible:=True;
  DoaNo:=6;
end;

procedure TfrmAdeieh.lbl7MojirClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\7.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img7Mojir.Visible:=True;
  DoaNo:=7;
end;

procedure TfrmAdeieh.lbl8JameeClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\8.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img8Jamee.Visible:=True;
  DoaNo:=8;
end;

procedure TfrmAdeieh.lbl9AmirClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\9.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img9Amir.Visible:=True;
  DoaNo:=9;
end;

procedure TfrmAdeieh.lbl10ShabaniehClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\10.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img10Shabanieh.Visible:=True;
  DoaNo:=10;
end;

procedure TfrmAdeieh.lbl11AbooHamzehClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\11.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img11AbooHamzeh.Visible:=True;
  DoaNo:=11;
end;

procedure TfrmAdeieh.lbl12KomeilClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\12.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img12Komeil.Visible:=True;
  DoaNo:=12;
end;

procedure TfrmAdeieh.lbl13SamatClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\13.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img13Samat.Visible:=True;
  DoaNo:=13;
end;

procedure TfrmAdeieh.lbl14NodbehClick(Sender: TObject);
begin
  lblPlayPause.Visible:=True;
  lblStop.Visible:=True;
  MediaPlayer1.FileName:='Datas\14.mp3';
  MediaPlayer1.Open;
  CloseB:=False;
  MediaPlayer1.Play;
  imgPause.Visible:=True;
  Unvisible;
  img14Nodbeh.Visible:=True;
  DoaNo:=14;
end;

procedure TfrmAdeieh.Unvisible;
begin
  img1Salamati.Visible:=False;
  img2Yasin.Visible:=False;
  img3Amin.Visible:=False;
  img4Ashoora.Visible:=False;
  img5Tavassol.Visible:=False;
  img6Ahd.Visible:=False;
  img7Mojir.Visible:=False;
  img8Jamee.Visible:=False;
  img9Amir.Visible:=False;
  img10Shabanieh.Visible:=False;
  img11AbooHamzeh.Visible:=False;
  img12Komeil.Visible:=False;
  img13Samat.Visible:=False;
  img14Nodbeh.Visible:=False;
end;

end.
